//添加收货地址
const app = getApp(); //获取全局app.js

Page({
    //页面的初始数据
    data: {
        name: '', //姓名
        mobile: '', //手机号
        region: [], //开户行地区
        areaId: '', //开户行地区ID
        address: '', //详细地址
        is_def: 1, //是否默认
        pointlog:[],
        page:1,
    },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    var that = this;
      this.getPointLog();
  },
    getPointLog: function(){
      var that = this;
        var userid =app.db.get('userInfo').id;
        var page = that.data.page;
        var data = {
            userid: userid,
          page: page
        };
        app.api.pointLog(data,function(res){          
            if(res){
              var arr = [];
              for (var i in res.data) {
                arr.push(res.data[i])
              }
              that.setData({
                    pointLog: arr,
                    page: page+1,
                });
            }
        });
    },

    //下拉刷新
    onPullDownRefresh: function () {
      wx.stopPullDownRefresh();
    },

  onReachBottom:function(){
      var that = this;
      var userid = app.db.get('userInfo').id;
      var page = that.data.page;
      var data = {
        userid: userid,
        page: page
      };
      app.api.pointLog(data, function (res) {
        if (res) {
          // var arr = [];
          // for (var i in res.data) {
          //   arr.push(res.data[i])
          // }
          var arr = that.data.pointLog;
          for (var i in res.data) {
            arr.push(res.data[i])
          }
          that.setData({
            pointLog: arr,
            page: page + 1,
          });
        }
      });
    }
  


});