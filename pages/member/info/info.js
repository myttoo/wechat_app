const app = getApp(); //获取全局app.js
var date = new Date();
Page({
  data: {
    tel:'',
    nickname: '' ,
    birthday: "选择出生日期",
    sex: 0,
    endtime: date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() ,
    objectSex:[
      {
        id:3,
        name:'未知'
      },
      {
        id: 1,
        name: '男'
      },
      {
        id: 2,
        name: '女'
      },
    ],
    
  },
  //页面加载处理
  onLoad: function () {
    var page = this;
    var userid = app.db.get('userInfo').id;
    var data = {
      userid : userid
    }
    app.api.userInfo(data,function (res) {
      console.log(res);
      if (res.status) {
        var the_sex = 0;
        if(res.data.sex == 3){
          the_sex = 0;
        }else{
          the_sex = res.data.sex;
        }
        if(res.data.birthday == null){
          res.data.birthday = '请选择';
        }
        page.setData({
          nickname : res.data.nickname,
          sex : the_sex,
          birthday: res.data.birthday,
          tel:res.data.mobile
        });
      }else{
        //报错了

      }
    });
  },

  //下拉刷新
  onPullDownRefresh: function () {
      wx.stopPullDownRefresh();
  }, 
  
  //提交按钮
  showTopTips: function () {
    var userid = app.db.get('userInfo').id;
    if(this.data.nickname == ''){
      wx.showToast({
        title: '请输入昵称',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if (!this.ValidatePhone(this.data.tel)){
      wx.showToast({
        title: '手机号码格式不正确',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    var the_sex = this.data.sex;
    if (the_sex== 0) {
      the_sex = 3;
    }
    var data = {
      sex:the_sex,
      birthday:this.data.birthday,
      nickname: this.data.nickname,
      tel: this.data.tel,
      userid: userid
    };
    app.api.userEditInfo(data,function (res) {
      if (res.status) {
        app.common.successToShow();
        wx.navigateTo({
          url: '../index/index'
        })
      } else {
        app.common.errorToBack(res.msg,0);
      }
    });

  },
  nicknameChange: function (e) {
    this.setData({
      nickname: e.detail.value
    })
  },
  //生日选择后的触发事件
  bindDateChange: function (e) {
    this.setData({
      birthday: e.detail.value
    })
  },
  //性别选择后的触发事件
  bindSexChange: function (e) {
    this.setData({
      sex: e.detail.value,
    })
  },

  telChange: function (e) {
    this.setData({
      tel: e.detail.value
    })
  },

  ValidatePhone: function (val) {
    var isPhone = /^([0-9]{3,4}-)?[0-9]{7,8}$/;//手机号码
    var isMob = /^0?1[3|4|5|8][0-9]\d{8}$/;// 座机格式
    if(isMob.test(val) || isPhone.test(val)){
      return true;
    }
    else {
      return false;
    }
},

  
});