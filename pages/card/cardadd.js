//修改收货地址
const app = getApp(); //获取全局app.js

Page({
    //页面的初始数据
    data: {
        card: '', //卡号
        flag: true,
        failflag: true
    },


    //生命周期函数--监听页面加载
    onLoad: function (options) {
      var userInfo = app.db.get('userInfo');
      if (!userInfo) {
        wx.navigateTo({
          url: '../index/index',
        });
      }

      if (!userInfo.mobile) {
        wx.showModal({
          title: '提示',
          content: '请完善您的个人信息',
          success(res) {
            if (res.confirm) {
              wx.navigateTo({
                url: '../member/info/info',
              });
            } else if (res.cancel) {
              wx.navigateTo({
                url: '../member/info/info',
              });
            }
          }
        })
       
      }
        let id = options.id;
    },   

    hide: function () {
        wx.navigateTo({
            url: '../card/card'
        });

    },

    getCard: function (e) {
      let card = e.detail.value;
      this.setData({
        card: card
      });
    },

    failflaghide: function () {
        wx.navigateTo({
            url: '../card/cardadd'
        });
    },

    //提交按钮
    addCard: function () {
      var self = this;
      var userInfo = app.db.get('userInfo');
      if (!userInfo) {
        wx.navigateTo({
          url: '../index/index',
        });
        return false;
      }
      if (!userInfo.mobile) {
        wx.showModal({
          title: '提示',
          content: '请绑定手机号',
          success(res) {
            if (res.confirm) {
              wx.navigateTo({
                url: '../member/info/info',
              });
            } else if (res.cancel) {
              wx.navigateTo({
                url: '../member/info/info',
              });
            }
          }
        })
        return false;
      }
      else{
        if(this.data.card == ''){
            wx.showToast({
                title: '请输入卡号',
                icon: 'fail',
                duration: 2000
            })
            return false;
        }
        var cardnumber = parseInt(this.data.card);
        if (isNaN(cardnumber)){
          wx.showToast({
            title: '卡号格式不正确',
            icon: 'none',
            duration: 2000
          })
          return false;
        }           
        var data = {
            userid:app.db.get('userInfo').id,
            card: cardnumber
        };
        app.api.addCard(data, function (res) {            
            if (res ===1) {
                self.setData({ flag: false });
            } else {
                self.setData({ failflag: false });
            }
            setTimeout(function () {
                wx.navigateBack(1);
            }, 1500);
        });
      }

    },

});