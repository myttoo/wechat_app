//添加收货地址
const app = getApp(); //获取全局app.js

Page({
    //页面的初始数据
    data: {
        name: '', //姓名
        mobile: '', //手机号
        region: [], //开户行地区
        areaId: '', //开户行地区ID
        address: '', //详细地址
        is_def: 1, //是否默认
        cardlist:[],
    },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    var that = this;
      this.getCardList();
  },
    getCardList: function(){
        var userid =app.db.get('userInfo').id;
        var data = {
            userid: userid,
        };
        var page = this;
        app.api.myCards(data,function(res){
            var arr =[];
            for(var i in res){
                arr.push(res[i])
            }
            if(res){
                page.setData({
                    cardlist: arr,
                    // ajaxStatus: false,
                });
                console.log(page.data.cardlist);
            }
        });
    },


    //添加卡片
    cardadd: function () {
        wx.navigateTo({
            url: '../card/cardadd'
        });
    },

    //下拉刷新
    onPullDownRefresh: function () {
        wx.stopPullDownRefresh();
    }, 

});