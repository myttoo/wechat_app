//index.js
//获取应用实例
// var utils = require('../../utils/util.js');
const app = getApp()

Page({
    data: {
        msg:'',
        showcheck: 1,
        qrcode:'',
        motto: '',
        articlelist:[],
        cardlist:[],
        // mygoodslist:[],
        userInfo: {},
        hasUserInfo: false,
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
        nickname: '',
        point: 0, //用户积分
        balance: '0.00', //用户余额
        isPoint: true, //开启积分
        avatar: '../../image/default_avatar.png',
        bindMobile: false,
        statusData: [], //状态数据
        isClerk: false, //是不是店员
        winWidth: 0,
        winHeight: 0,
        // tab切换
        currentTab: 0,
        newsid: [],
        searchData: {
            where: {},
            limit: app.config.list_limit,
            page: 1,
            order: {
                key: 'id',
                sort: 'desc'
            }
        },
        modalHidden: true,
        modalHidden1: true,
        modalHidden2: true,
    },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function (query) {
    var that = this;    
    wx.getSystemInfo( {
      success: res =>  {
        app.globalData.systemInfo = res.systemInfo
        that.setData( {
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }
    });
    // var TIME = util.formatTime(new Date());
    // this.setData({    
    // time: TIME,
    // });
    this.getArticles();
      this.getWxCode(function(code){
          var data = {
              code: code
          };
          app.api.login1(data, function (res) {
              if(!res.status){
                  wx.showToast({
                      title: res.msg,
                      icon: 'success',
                      duration: 2000,
                      success: function (res) {
                          wx.navigateBack({
                              delta: 1
                          })
                      }
                  })
              }else{
                if (query.scene) {
                  let scene = decodeURIComponent(query.scene);
                  let type = scene.split(",")[0];
                  let id = scene.split(',')[1];
                  // let type = options.type;
                  // let id = options.id;
                  console.log(type);
                  console.log(id);
                  console.log(res.id);
                  var data ={
                    type: type,
                    id: id,
                    admin: res.id
                  }
                  app.api.adminOperate(data, function (res) {

                    that.setData({
                      msg: res,
                      modalHidden2: false
                    });
                    // wx.showToast({
                    //   title: res,
                    //   icon: 'success',
                    //   duration: 5000
                    // })
                    // console.log(res)
                    
                  });
                }
                app.db.set('userInfo',res);
                that.setData({
                  userInfo: res,
                });
                that.showcheck();       
                app.globalData.userInfo = res;
                 
                  that.getMygoods();
                  that.getCardList();
                  return false;

              }
          });
      });

      //   if (app.globalData.userInfo) {
  //     this.setData({
  //       userInfo: app.globalData.userInfo,
  //       hasUserInfo: true
  //     })
  //   } else if (this.data.canIUse){
  //     // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
  //     // 所以此处加入 callback 以防止这种情况
  //     app.userInfoReadyCallback = res => {
  //         console.log(res.userInfo);
  //         this.setData({
  //         userInfo: res.userInfo,
  //         hasUserInfo: true
  //       })
  //     }
  //   } else {
  //     // 在没有 open-type=getUserInfo 版本的兼容处理
     
  //   }
  },

//   onPullDownRefresh: function(){
//     wx.request({
//         url: '',
//         data: {},
//         method: 'GET',
//         success: function (res) {},
//         fail: function (res) {},
//         complete: function (res) {
//             wx.stopPullDownRefresh();
//         }
//     })
// },

  onShow: function () {
    this.refreshuser();
    this.getMygoods();
    this.getCardList();
  },

    getWxCode: function(callback){
        wx.login({
            success: function (res) {
                if (res.code) {
                    callback(res.code);
                    return res.code;
                } else {
                    //wx.login成功，但是没有取到code
                    wx.showToast({
                        title: '未取得code',
                        icon: 'warn',
                        duration: 2000
                    })
                }
            },
            fail: function (res) {
                //wx.login的fail
                wx.showToast({
                    title: '用户授权失败wx.login',
                    icon: 'warn',
                    duration: 2000
                })
            }
        });
    },

    getArticles:function(){
        var data = {
            type_id: 1,
            limit:100
        }
        var page = this;
        app.api.getarticleList(data, function (res) {
            for (var j = 0; j < res.data.list.length; j++) {
                res.data.list[j].rand = Math.floor(Math.random()*3+1);
                res.data.list[j].utime = app.common.timeToDate(res.data.list[j].utime)
            }
            page.setData({
                articlelist:res.data.list
            });
        });
    },

    goodsdetail: function (e) {
        wx.navigateTo({
            url: '../other/detail/detail?id=' + e.target.dataset.id
        });
    },


    //取得商品数据
    getMygoods: function(){
        var userid = this.data.userInfo.id;
        var data = {
            userid: userid,
        };
        var page = this;
        app.api.myGoods(data,function(res){
            var arr =[];
            for(var i in res){
                arr.push(res[i])
            }
            if(res){
                page.setData({
                    mygoodsList: arr,
                    // ajaxStatus: false,
                });
            }
        });
    },

    //取得运动数据
    getCardList: function(){
        var userid =app.db.get('userInfo').id;
        var data = {
            userid: userid,
        };
        var page = this;
        app.api.myCards(data,function(res){
          if (res !='') {
            var arr =[];
            for(var i in res){
              res[i].category1 = page.exchangecat(res[i].category);
              res[i].type1 = page.exchangetype(res[i].type);
              arr.push(res[i])
            }
                page.setData({
                    cardlist: arr,
                });
            }
        });
    },

    exchangecat:function(data){
      var cat = '';       
      if (data == 'pingpangqiu') {
        cat = '乒乓球';
        return cat;
      }
      if (data == 'yumaoqiu') {
        cat = '羽毛球';
        return cat;
      }
      if (data == 'lanqiu') {
        cat = '篮球';
        return cat;
      }
      if (data == 'zuqiu') {
        cat = '足球';
        return cat;
      }
      if (data == 'youyong') {
        cat = '游泳';
        return cat;
      }
      if (data == 'jianshen') {
        cat = '健身';
        return cat;
      }
      if (data == 'tongyong') {
        cat = '通用';
        return cat;
      }
    },

  exchangetype: function (data) {
    var type = '';
    if (data == 'nianka') {
      type = '年卡';
      return type;
    }
    if (data == 'yueka') {
      type = '月卡';
      return type;
    }
  },


    

  getUserInfo: function(e) {
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
    /**
     * 滑动切换tab
     */
    bindChange: function( e ) {
 
      var that = this;
      that.setData( { currentTab: e.detail.current });
   
    },
    /**
     * 点击tab切换
     */
    swichNav: function( e ) {
   
      var that = this;
   
      if( this.data.currentTab === e.target.dataset.current ) {
        return false;
      } else {
        that.setData( {
          currentTab: e.target.dataset.current
        })
      }
    },
    //活动文章
       article: function () {
        // var id =  e.currentTarget.dataset.id;
        wx.navigateTo({
            // url: '../other/article/article?id='+id
            url: '../other/article/article'
        });
    },

    //绑定会员卡
    cardadd: function () {
        // var id =  e.currentTarget.dataset.id;
        wx.navigateTo({
            // url: '../goods/detail/detail?id='+id
            url: '../card/cardadd'
        });
    },

    articledetail: function (e) {
        wx.navigateTo({
            url: '../other/article/article?id=' + e.target.dataset.id
        });
    },

    checkbutton: function (e) {
      let page = this;
      var userid = e.target.dataset.id;
      if (!userid){
        this.onLoad();
      }
      let data = {
        'type': 'check',
        'id': userid
      }
      app.api.getQRCode(data, function (e) {
        if (e.status) {
          let url = app.config.api_url + e.data;
          page.setData({
            qrcode: url,
            modalHidden1: false
          });
        } else {
          page.setData({
            qrcodeErrorMsg: e.msg
          });
        }
      });
    },

    buttonTap: function(e) {
        let page = this;
        let data = {
            'type': 'goods',
            'id': e.target.dataset.id
        }
        app.api.getQRCode(data, function (e) {
            if (e.status) {
                let url = app.config.api_url + e.data;
                page.setData({
                    qrcode: url,
                    modalHidden: false
                });
            } else {
                page.setData({
                    qrcodeErrorMsg: e.msg
                });
            }
        });
    },
    //下拉刷新
    // onPullDownRefresh: function () {
    //     this.getUserInfo();
    //     this.getMygoods();
    //     this.getCardList();
    //     complete: function (res) {
    //         wx.stopPullDownRefresh();
    //     }
    // },
    onPullDownRefresh: function () {
        this.getUserInfo();
        this.getMygoods();
        this.getCardList();
        this.showcheck();
        // this.showcheck(that.data.userInfo.checktime);
        wx.stopPullDownRefresh();
      },  
    /**
     * 点击取消
     */
    modalCandel: function() {
      var that = this;
      this.getMygoods();
      this.refreshuser();
        this.setData({
            modalHidden: true,
            modalHidden1: true,
            modalHidden2: true
        })
    },

    /**
     *  点击确认
     */
    modalConfirm: function() {
      var that = this;
      this.getMygoods();
      this.refreshuser();
        this.setData({
            modalHidden: true,
            modalHidden1: true,
            modalHidden2: true
        })
    },

    showcheck:function(){
      var time = this.data.userInfo.checktime;
      console.log(time);
      var old_date = new Date(time * 1000);
      var year2 = old_date.getFullYear();
      var month2 = old_date.getMonth() + 1;
      var date2 = old_date.getDate();

      var now = new Date();
      var year = now.getFullYear();
      var month = now.getMonth()+1;
      var date = now.getDate();               

      var old_date2 = year2 + "/" + month2 + "/" + date2;

      var new_date = year + "/" + month + "/" + date;
      var old_date3 = Date.parse(old_date2);
      var new_date2 = Date.parse(new_date);

      // console.log(old_date3);
      // console.log(new_date2);

      if (new_date2 <= old_date3){
          this.setData({
            showcheck:false
          })
        };
      // };
    },

    refreshuser:function(){
      var that = this;
      if (app.db.get('userInfo').id){
        var data = {
          id: app.db.get('userInfo').id
        };
        app.api.refreshuser(data,function(res){
          app.db.set('userInfo', res);
          app.globalData.userInfo = res;
          that.setData({
            userInfo: res
          }); 
          that.showcheck();
        })
      }
    },

    onReachBottom:function(){
      var userid = this.data.userInfo.id;
      var data = {
          userid: userid,
      };
      var page = this;
      app.api.myGoods(data,function(res){
          var arr =[];
          for(var i in res){
              arr.push(res[i])
          }
          if(res){
              page.setData({
                  mygoodsList: arr,
                  // ajaxStatus: false,
              });
          }
      });
    },

})


